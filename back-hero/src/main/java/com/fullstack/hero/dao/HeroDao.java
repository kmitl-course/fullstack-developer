package com.fullstack.hero.dao;

import com.fullstack.hero.dto.HeroDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class HeroDao {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<HeroDto> findAllHeroes(HeroDto heroDto){

        StringBuilder sql = new StringBuilder();
        Map<String,Object> params = new HashMap<>();

        sql.append(" select heroes.hero_id, heroes.name,heroes.description,DATE_FORMAT(heroes.birthday,'%Y-%m-%d') birthday,  ");
        sql.append("        skill.skill_id, skill.name skill_name ");
        sql.append(" from heroes ");
        sql.append(" left outer join skill on heroes.skill_id = skill.skill_id ");
        sql.append(" where 1=1 ");
        if(!StringUtils.isEmpty(heroDto.getName())){
            sql.append(" and heroes.name like :hero_name ");
            params.put("hero_name","%"+heroDto.getName()+"%");
        }
        if(!StringUtils.isEmpty(heroDto.getSkillId())){
            sql.append(" and skill.skill_id = :skill_id ");
            params.put("skill_id", heroDto.getSkillId());
        }
        sql.append(" order by heroes.hero_id ");

        List<HeroDto> result = namedParameterJdbcTemplate.query(sql.toString(),params,new BeanPropertyRowMapper<>(HeroDto.class));

        return result;
    }

    public HeroDto findHeroById(HeroDto hero){

        StringBuilder sql = new StringBuilder();
        sql.append(" select hero_id,name,description,DATE_FORMAT(birthday,'%Y-%m-%d') birthday,skill_id " );
        sql.append(" from heroes where hero_id=:hero_id ");

        Map<String,Object> params = new HashMap<>();
        params.put("hero_id",hero.getHeroId());

        HeroDto result = namedParameterJdbcTemplate.queryForObject(sql.toString(),params,new BeanPropertyRowMapper<>(HeroDto.class));

        return result;
    }

    public int insertHero(HeroDto hero){

        StringBuilder sql = new StringBuilder();
        sql.append(" INSERT INTO heroes(hero_id, name, description,birthday,skill_id) " );
        sql.append(" VALUES (:hero_id, :name, :description,STR_TO_DATE(:birthday, '%Y-%m-%d'),:skill_id) ");

        Map<String,Object> params = new HashMap<>();
        params.put("hero_id", hero.getHeroId());
        params.put("name", hero.getName());
        params.put("description", hero.getDescription());
        params.put("birthday", hero.getBirthday());
        params.put("skill_id", hero.getSkillId());

        int numRow = namedParameterJdbcTemplate.update(sql.toString(),params);

        return numRow;
    }

    public int updateHero(HeroDto hero){

        StringBuilder sql = new StringBuilder();
        sql.append(" update heroes set " );
        sql.append("    name = :name, " );
        sql.append("    description = :description, " );
        sql.append("    birthday = STR_TO_DATE(:birthday, '%Y-%m-%d'), " );
        sql.append("    skill_id = :skill_id " );
        sql.append(" where hero_id = :hero_id " );

        Map<String,Object> params = new HashMap<>();
        params.put("hero_id", hero.getHeroId());
        params.put("name", hero.getName());
        params.put("description", hero.getDescription());
        params.put("birthday", hero.getBirthday());
        params.put("skill_id", hero.getSkillId());

        int numRow = namedParameterJdbcTemplate.update(sql.toString(),params);

        return numRow;
    }

    public int deleteHero(HeroDto hero){
        StringBuilder sql = new StringBuilder();
        sql.append(" delete from heroes where hero_id = :hero_id " );

        Map<String,Object> params = new HashMap<>();
        params.put("hero_id", hero.getHeroId());

        int numRow = namedParameterJdbcTemplate.update(sql.toString(),params);

        return numRow;
    }
}
