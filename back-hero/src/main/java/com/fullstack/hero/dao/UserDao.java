package com.fullstack.hero.dao;

import com.fullstack.hero.dto.UserDto;
import com.fullstack.hero.dto.UserTokenDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class UserDao {
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UserDto findUserByUsernameAndPassword(String username, String password){

        StringBuilder sql = new StringBuilder();
        Map<String,Object> params = new HashMap<>();
        sql.append(" select id,username,status from users where username=:username and password=:password ");

        params.put("username",username);
        params.put("password",password);

        return namedParameterJdbcTemplate.queryForObject(sql.toString(),params,new BeanPropertyRowMapper<>(UserDto.class));
    }

    public UserDto findUserByToken(String token){

        StringBuilder sql = new StringBuilder();
        Map<String,Object> params = new HashMap<>();
        sql.append(" select user_id id,status from user_token where token=:token and status = :status ");

        params.put("token",token);
        params.put("status","A");

        return namedParameterJdbcTemplate.queryForObject(sql.toString(),params,new BeanPropertyRowMapper<>(UserDto.class));
    }

    public Integer saveUserToken(UserTokenDto userTokenDto){
        StringBuilder sql = new StringBuilder();
        Map<String,Object> params = new HashMap<>();
        sql.append(" insert into user_token(user_id,token,status) values(:user_id,:token,:status) ");

        params.put("user_id",userTokenDto.getId());
        params.put("token",userTokenDto.getToken());
        params.put("status",userTokenDto.getStatus());

        return namedParameterJdbcTemplate.update(sql.toString(),params);
    }
}
