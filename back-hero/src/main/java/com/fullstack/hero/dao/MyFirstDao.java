package com.fullstack.hero.dao;

import com.fullstack.hero.dto.HeroDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class MyFirstDao {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<HeroDto> findAllHero(){
        StringBuilder sql = new StringBuilder();
        sql.append(" select * from hero ");

        Map<String,Object> params = new HashMap<String,Object>();

        return namedParameterJdbcTemplate.query(sql.toString(),params,new BeanPropertyRowMapper<>(HeroDto.class));
    }

}
