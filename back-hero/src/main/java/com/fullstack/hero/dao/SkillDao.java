package com.fullstack.hero.dao;

import com.fullstack.hero.dto.HeroDto;
import com.fullstack.hero.dto.SkillDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SkillDao {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<SkillDto> findAllSkill(){
        StringBuilder sql = new StringBuilder();
        sql.append(" select skill_id,name,description from skill ");

        Map<String,Object> params = new HashMap<>();

        List<SkillDto> result = namedParameterJdbcTemplate.query(sql.toString(),params,new BeanPropertyRowMapper<>(SkillDto.class));

        return result;
    }

    public SkillDto findSkillById(SkillDto skill){

        StringBuilder sql = new StringBuilder();
        sql.append(" select skill_id,name,description from skill where skill_id=:skill_id ");

        Map<String,Object> params = new HashMap<>();
        params.put("skill_id",skill.getSkillId());

        SkillDto result = namedParameterJdbcTemplate.queryForObject(sql.toString(),params,new BeanPropertyRowMapper<>(SkillDto.class));

        return result;
    }

    public int insertHero(SkillDto skill){

        StringBuilder sql = new StringBuilder();
        sql.append(" INSERT INTO skill(skill_id, name, description) " );
        sql.append(" VALUES (:skill_id, :name, :description) ");

        Map<String,Object> params = new HashMap<>();
        params.put("skill_id", skill.getSkillId());
        params.put("name", skill.getName());
        params.put("description", skill.getDescription());

        int numRow = namedParameterJdbcTemplate.update(sql.toString(),params);

        return numRow;
    }

    public int updateHero(SkillDto skill){

        StringBuilder sql = new StringBuilder();
        sql.append(" update skill set " );
        sql.append("    name = :name, " );
        sql.append("    description = :description " );
        sql.append(" where skill_id = :skill_id " );

        Map<String,Object> params = new HashMap<String,Object>();
        params.put("skill_id", skill.getSkillId());
        params.put("name", skill.getName());
        params.put("description", skill.getDescription());

        int numRow = namedParameterJdbcTemplate.update(sql.toString(),params);

        return numRow;
    }

}
