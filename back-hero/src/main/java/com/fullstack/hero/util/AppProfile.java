package com.fullstack.hero.util;

import com.fullstack.hero.dto.UserDto;
import org.springframework.stereotype.Component;

@Component
public class AppProfile {
    private UserDto userDto;

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }
}
