package com.fullstack.hero.filter;

import com.fullstack.hero.dao.UserDao;
import com.fullstack.hero.dto.CommonResponseDto;
import com.fullstack.hero.dto.UserDto;
import com.fullstack.hero.dto.UserTokenDto;
import com.fullstack.hero.service.TokenProvider;
import com.fullstack.hero.util.AppProfile;
import io.jsonwebtoken.SignatureException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class SecurityFilter implements Filter {

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private AppProfile appProfile;

    @Autowired
    private UserDao userDao;

    @Override
    public void doFilter(ServletRequest request, ServletResponse respond, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest)request;
        HttpServletResponse httpRespond = (HttpServletResponse)respond;

        httpRespond.addHeader("Access-Control-Allow-Origin", "*");
        httpRespond.addHeader("Access-Control-Allow-Methods","GET, OPTIONS, HEAD, PUT, POST");
        httpRespond.addHeader("Access-Control-Allow-Headers","*");

        if (httpRequest.getMethod().equals("OPTIONS")) {
            httpRespond.setStatus(HttpServletResponse.SC_OK);
            return;
        }

        String requestUrl = httpRequest.getRequestURI();
        if(requestUrl.startsWith("/api/login")){
            filterChain.doFilter(request, respond);
            return;
        }

        String userToken = httpRequest.getHeader("Token");
        if(null != userToken){
            try{
                appProfile.setUserDto(tokenProvider.parseToken(userToken));
                filterChain.doFilter(request, respond);

//                UserDto userDto = userDao.findUserByToken(userToken);
//                if(null != userDto) {
//                    filterChain.doFilter(request, respond);
//                }else{
//                    httpRespond.sendError(HttpServletResponse.SC_UNAUTHORIZED,"");
//                }
            } catch (SignatureException e){
                httpRespond.sendError(HttpServletResponse.SC_UNAUTHORIZED,"");
            } catch (Exception e){
                httpRespond.sendError(HttpServletResponse.SC_UNAUTHORIZED,"");
            }
        }else{
            httpRespond.sendError(HttpServletResponse.SC_UNAUTHORIZED,"");
        }
    }
}
