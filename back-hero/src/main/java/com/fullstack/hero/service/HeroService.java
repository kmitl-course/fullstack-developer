package com.fullstack.hero.service;

import com.fullstack.hero.dao.HeroDao;
import com.fullstack.hero.dto.CommonResponseDto;
import com.fullstack.hero.dto.HeroDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class HeroService {

    @Autowired
    private HeroDao heroDao;

    public CommonResponseDto findHero(HeroDto heroDto){
        CommonResponseDto response = new CommonResponseDto();
        response.setStatusCode("200");
        try {
            response.setData(heroDao.findHeroById(heroDto));
        }catch (EmptyResultDataAccessException e){
            response.setStatusCode("404");
        }
        return response;
    }

    public CommonResponseDto findHeroes(HeroDto heroDto){
        CommonResponseDto response = new CommonResponseDto();
        response.setStatusCode("200");
        response.setData(heroDao.findAllHeroes(heroDto));
        return response;
    }

    public CommonResponseDto insertHero(HeroDto heroDto){
        CommonResponseDto response = new CommonResponseDto();
        try {
            int rowNum = heroDao.insertHero(heroDto);
            if (rowNum > 0) {
                response.setStatusCode("200");
            } else {
                response.setStatusCode("404");
            }
        }catch (DuplicateKeyException e){
            response.setStatusCode("409");
        }
        return response;
    }

    public CommonResponseDto updateHero(HeroDto heroDto){
        CommonResponseDto response = new CommonResponseDto();
        int rowNum = heroDao.updateHero(heroDto);
        if(rowNum > 0){
            response.setStatusCode("200");
        }else{
            response.setStatusCode("404");
        }
        return response;
    }

    public CommonResponseDto deleteHero(HeroDto heroDto){
        CommonResponseDto response = new CommonResponseDto();
        int rowNum = heroDao.deleteHero(heroDto);
        if(rowNum > 0){
            response.setStatusCode("200");
        }else{
            response.setStatusCode("404");
        }
        return response;
    }
}
