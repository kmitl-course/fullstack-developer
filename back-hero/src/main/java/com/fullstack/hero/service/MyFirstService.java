package com.fullstack.hero.service;

import com.fullstack.hero.dto.HeroDto;
import org.springframework.stereotype.Service;

@Service
public class MyFirstService {

    public HeroDto getHero(){
        HeroDto heroDto = new HeroDto();
        heroDto.setHeroId("01");
        heroDto.setName("Hello Hero");

        return heroDto;
    }
}
