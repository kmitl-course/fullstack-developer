package com.fullstack.hero.service;

import com.fullstack.hero.dao.UserDao;
import com.fullstack.hero.dto.CommonResponseDto;
import com.fullstack.hero.dto.LoginDto;
import com.fullstack.hero.dto.UserDto;
import com.fullstack.hero.dto.UserTokenDto;
import com.fullstack.hero.util.HashUtil;
import io.jsonwebtoken.SignatureException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;

@Service
public class LoginService {
    @Autowired
    private HashUtil hashUtil;
    @Autowired
    private UserDao userDao;
    @Autowired
    private TokenProvider tokenProvider;

    public CommonResponseDto login(LoginDto loginDto){
        CommonResponseDto commonResponseDto = new CommonResponseDto();
        try {
            String hashPassword = hashUtil.getHashPassword(loginDto.getPassword());
            UserDto userDto = userDao.findUserByUsernameAndPassword(loginDto.getUsername(),hashPassword);

            UserTokenDto userTokenDto = new UserTokenDto();
            userTokenDto.setId(userDto.getId());
            userTokenDto.setToken(tokenProvider.generateUserToken(userDto));
            userTokenDto.setStatus("A");

            userDao.saveUserToken(userTokenDto);

            commonResponseDto.setStatusCode("200");
            commonResponseDto.setData(userTokenDto);
        }catch (NoSuchAlgorithmException e) {
            commonResponseDto.setStatusCode("500");
        }catch (EmptyResultDataAccessException e){
            commonResponseDto.setStatusCode("404");
        }

        return commonResponseDto;
    }

    public CommonResponseDto verify(UserTokenDto tokenDto){
        CommonResponseDto commonResponseDto = new CommonResponseDto();
        try{
            UserDto userDto = tokenProvider.parseToken(tokenDto.getToken());
            commonResponseDto.setStatusCode("200");
            commonResponseDto.setData(userDto);
        }catch (SignatureException e){
            commonResponseDto.setStatusCode("403");
        }
        return commonResponseDto;
    }
}
