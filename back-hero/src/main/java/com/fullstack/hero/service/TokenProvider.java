package com.fullstack.hero.service;

import com.fullstack.hero.dto.UserDto;
import com.fullstack.hero.property.CustomProperty;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@Component
public class TokenProvider {

    @Autowired
    private CustomProperty customProperty;

    public String generateUserToken(UserDto userDto){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        Map<String, Object> claims = new HashMap<>();
        claims.put("username", userDto.getUsername());
        claims.put("status", userDto.getStatus());
        claims.put("current_date", dateFormat.format(Calendar.getInstance().getTime()));

        return Jwts.builder().setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, customProperty.getSecretKey())
                .compact();
    }

    public UserDto parseToken(String token){

        Claims claims = Jwts.parser()
                .setSigningKey(customProperty.getSecretKey())
                .parseClaimsJws(token)
                .getBody();

        UserDto userDto = new UserDto();
        userDto.setUsername(claims.get("username").toString());
        userDto.setStatus(claims.get("status").toString());

        return userDto;
    }
}
