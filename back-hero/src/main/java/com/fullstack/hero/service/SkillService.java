package com.fullstack.hero.service;

import com.fullstack.hero.dao.SkillDao;
import com.fullstack.hero.dto.CommonResponseDto;
import com.fullstack.hero.dto.SkillDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class SkillService {

    @Autowired
    private SkillDao skillDao;

    public CommonResponseDto findSkill(SkillDto skillDto){
        CommonResponseDto response = new CommonResponseDto();
        response.setStatusCode("200");
        if(null!=skillDto){
            try {
                response.setData(skillDao.findSkillById(skillDto));
            }catch (EmptyResultDataAccessException e){
                response.setStatusCode("404");
            }
        }else {
            response.setData(skillDao.findAllSkill());
        }
        return response;
    }

    public CommonResponseDto insertSkill(SkillDto skillDto){
        CommonResponseDto response = new CommonResponseDto();
        try {
            int rowNum = skillDao.insertHero(skillDto);
            if (rowNum > 0) {
                response.setStatusCode("200");
            } else {
                response.setStatusCode("404");
            }
        }catch (DuplicateKeyException e){
            response.setStatusCode("409");
        }
        return response;
    }

    public CommonResponseDto updateSkill(SkillDto skillDto){
        CommonResponseDto response = new CommonResponseDto();
        int rowNum = skillDao.updateHero(skillDto);
        if(rowNum > 0){
            response.setStatusCode("200");
        }else{
            response.setStatusCode("404");
        }
        return response;
    }
}
