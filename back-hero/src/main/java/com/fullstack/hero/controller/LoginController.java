package com.fullstack.hero.controller;

import com.fullstack.hero.dto.CommonResponseDto;
import com.fullstack.hero.dto.LoginDto;
import com.fullstack.hero.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("login")
    public CommonResponseDto login(@RequestBody LoginDto request){
        return loginService.login(request);
    }
}
