package com.fullstack.hero.controller;

import com.fullstack.hero.dto.CommonResponseDto;
import com.fullstack.hero.dto.SkillDto;
import com.fullstack.hero.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class SkillController {

    @Autowired
    private SkillService skillService;

    @GetMapping("/skills")
    public CommonResponseDto findAllHeroes(){
        return skillService.findSkill(null);
    }

    @GetMapping("/skill")
    public CommonResponseDto findHeroById(@RequestParam String skillId){
        SkillDto skillDto = new SkillDto();
        skillDto.setSkillId(skillId);
        return skillService.findSkill(skillDto);
    }

    @PostMapping("/skill")
    public CommonResponseDto createHero(@RequestBody SkillDto body){
        return skillService.insertSkill(body);
    }

    @PatchMapping("/skill")
    public CommonResponseDto updateHero(@RequestBody SkillDto body){
        return skillService.updateSkill(body);
    }
}
