package com.fullstack.hero.controller;

import com.fullstack.hero.dto.CommonResponseDto;
import com.fullstack.hero.dto.HeroDto;
import com.fullstack.hero.service.HeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class HeroController {

    @Autowired
    private HeroService heroService;

    @GetMapping("/heroes")
    public CommonResponseDto findAllHeroes(@RequestParam(required = false) String heroName,
                                           @RequestParam(required = false) String skillId){
        HeroDto heroDto = new HeroDto();
        heroDto.setName(heroName);
        heroDto.setSkillId(skillId);
        return heroService.findHeroes(heroDto);
    }

    @GetMapping("/hero")
    public CommonResponseDto findHeroById(@RequestParam String heroId){
        HeroDto heroDto = new HeroDto();
        heroDto.setHeroId(heroId);
        return heroService.findHero(heroDto);
    }

    @PostMapping("/hero")
    public CommonResponseDto createHero(@RequestBody HeroDto body){
        return heroService.insertHero(body);
    }

    @PatchMapping("/hero")
    public CommonResponseDto updateHero(@RequestBody HeroDto body){
        return heroService.updateHero(body);
    }

    @DeleteMapping("/hero")
    public CommonResponseDto deleteHero(@RequestBody HeroDto body){
        return heroService.deleteHero(body);
    }
}
