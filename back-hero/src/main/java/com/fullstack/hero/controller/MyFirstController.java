package com.fullstack.hero.controller;

import com.fullstack.hero.dto.HeroDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyFirstController {
    @GetMapping("hello")
    public HeroDto helloSpringBoot(){
        HeroDto heroDto = new HeroDto();
        heroDto.setHeroId("01");
        heroDto.setName("Hello Hero");
        return heroDto;
    }
}

