create table heroes
(
	hero_id varchar(10) not null
		primary key,
	name varchar(60) null,
	description varchar(200) null,
	birthday date null,
	skill_id varchar(2) null
);

create table skill
(
	skill_id varchar(2) not null
		primary key,
	name varchar(60) null,
	description varchar(200) null
);

create table user_token
(
	id int auto_increment
		primary key,
	user_id int not null,
	token varchar(250) not null,
	status varchar(1) not null
);

create table users
(
	id int auto_increment
		primary key,
	username varchar(250) not null,
	password varchar(250) not null,
	status varchar(1) not null,
	constraint users_username_uindex
		unique (username)
);

INSERT INTO users (id, username, password, status) VALUES (1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 'A');