import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CommonResponse} from "../interface/common-response";
import {environment} from "../../environments/environment";
import {Login} from "../interface/login";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  login(data:Login): Observable<CommonResponse> {
    return this.http.post<CommonResponse>(environment.apiUrl + '/api/login' ,data);
  }
}
