import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CommonResponse} from "../interface/common-response";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  constructor(private http: HttpClient) { }

  getSkills(): Observable<CommonResponse>{
    return this.http.get<CommonResponse>(environment.apiUrl + '/api/skills');
  }
}
