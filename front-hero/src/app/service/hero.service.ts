import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Hero} from "../interface/hero";
import {HttpClient, HttpHeaders} from "@angular/common/http";

import { environment } from '../../environments/environment';
import {CommonResponse} from "../interface/common-response";
import {HeroSearch} from "../interface/hero-search";

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  constructor(private http: HttpClient) { }

  getHeroes(hero?:HeroSearch): Observable<CommonResponse> {
    let queryString = '?';
    if(hero) {
      queryString += Object.keys(hero).map(key => key + '=' + hero[key]).join('&');
    }
    return this.http.get<CommonResponse>(environment.apiUrl + '/api/heroes' + queryString);
  }

  getHero(heroId:string): Observable<CommonResponse>{
    return this.http.get<CommonResponse>(environment.apiUrl + '/api/hero?heroId=' +heroId);
  }

  insertHero(data:Hero): Observable<CommonResponse> {
    return this.http.post<CommonResponse>(environment.apiUrl + '/api/hero' ,data);
  }

  updateHero(data:Hero): Observable<CommonResponse> {
    return this.http.patch<CommonResponse>(environment.apiUrl + '/api/hero',data);
  }
  deleteHero(data:Hero): Observable<CommonResponse> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: data
    };
    return this.http.delete<CommonResponse>(environment.apiUrl + '/api/hero',httpOptions);
  }
}
