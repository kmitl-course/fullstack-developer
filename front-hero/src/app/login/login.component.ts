import { Component, OnInit } from '@angular/core';
import {Login} from "../interface/login";
import {AuthService} from "../common/auth/auth.service";
import {Router} from "@angular/router";
import {LoginService} from "../service/login.service";
import Swal from 'sweetalert2'
import {Token} from "../interface/token";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginData:Login = {}

  constructor(private authService:AuthService,
              private router:Router,
              private loginService:LoginService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.loginService.login(this.loginData).subscribe(response=>{
      if(response.statusCode === '200'){
        let userToken:Token = response.data;

        this.authService.saveToken(userToken.token);
        this.router.navigate(['/']);
      }else{
        Swal.fire("Alert", 'User/Password incorrect!', 'error');
      }
    },error=>{
      Swal.fire("Alert", 'Login Error!', 'error');
    });
  }
}
