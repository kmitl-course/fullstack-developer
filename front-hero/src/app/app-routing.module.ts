import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HeroListComponent} from "./hero-list/hero-list.component";
import {HeroDetailComponent} from "./hero-detail/hero-detail.component";
import {AuthGuard} from "./common/auth/auth.guard";
import {LoginComponent} from "./login/login.component";
import {LogoutComponent} from "./logout/logout.component";

const routes: Routes = [
  {path: '', redirectTo: 'hero-list', pathMatch: 'full' },
  {path: 'login', component:LoginComponent },
  {path: 'logout', component:LogoutComponent },
  {path: 'hero-list', component:HeroListComponent, canActivate: [AuthGuard] },
  { path: 'hero-detail', component: HeroDetailComponent, canActivate: [AuthGuard]  },
  { path: 'hero-detail/:id', component: HeroDetailComponent, canActivate: [AuthGuard]  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
