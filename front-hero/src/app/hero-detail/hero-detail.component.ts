import { Component, OnInit } from '@angular/core';
import {Hero} from "../interface/hero";
import {HeroService} from "../service/hero.service";
import {ActivatedRoute, Router} from "@angular/router";
import {DatePipe} from "@angular/common";
import {SkillService} from "../service/skill.service";
import {Skill} from "../interface/skill";
import {LoadingScreenService} from "../common/loading-screen/loading-screen.service";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {
  birthday:Date;
  hero:Hero = {
    heroId:'',
    name:'',
    description:'',
    skillId:''
  };
  skills:Skill[] = [];

  isNew:boolean = true;

  constructor(private heroService:HeroService,
              private skillService:SkillService,
              private loadingScreenService:LoadingScreenService,
              private route:ActivatedRoute,
              private router: Router,
              private datePipe: DatePipe
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.hero.heroId = params['id'];
      if(this.hero.heroId){
        this.isNew = false;
        this.heroService.getHero(this.hero.heroId).subscribe(response=>{
          if(response.statusCode === '200') {
            this.hero = response.data;
            if (!this.hero.skillId) {
              this.hero.skillId = '';
            }
            if (this.hero.birthday) {
              this.birthday = new Date(this.hero.birthday);
            }
          }else if(response.statusCode === '404'){
            Swal.fire("Alert", 'Hero not found!', 'error');
            this.router.navigate(['/hero-list']);
          }else{
            Swal.fire("Alert", 'Something went wrong!', 'error');
          }
        },error => {
          Swal.fire("Alert", 'Something went wrong!', 'error');
        });
      }else{
        this.isNew = true;
      }
      console.log(this.hero.heroId);
    });
    this.skillService.getSkills().subscribe(response=>{
      this.skills = response.data;
    });
  }

  onBirthdayChange(){
    this.hero.birthday = this.datePipe.transform(this.birthday, 'yyyy-MM-dd');
  }

  onSubmit(){
    if(this.isNew){
      this.heroService.insertHero(this.hero).subscribe(response => {
        if (response.statusCode === '200') {
          Swal.fire("Alert", 'Create Successful!', 'success');
          this.router.navigate(['/hero-detail/'+ this.hero.heroId]);
        } else {
          Swal.fire("Alert", 'Create Fail!', 'error');
        }
      },error => {
        Swal.fire("Alert", 'Something went wrong!', 'error');
      });
    }else {
      this.heroService.updateHero(this.hero).subscribe(response => {
        if (response.statusCode === '200') {
          Swal.fire("Alert", 'Update Successful!', 'success');
          this.router.navigate(['/hero-detail/'+ this.hero.heroId]);
        } else {
          Swal.fire("Alert", 'Update Fail!', 'error');
        }
      },error => {
        Swal.fire("Alert", 'Something went wrong!', 'error');
      });
    }
  }

  onBlur() {
    this.heroService.getHero(this.hero.heroId).subscribe(response=>{
      if(response.statusCode === '200'){
        this.router.navigate(['/hero-detail/'+ this.hero.heroId]);
      }
    });

  }
}
