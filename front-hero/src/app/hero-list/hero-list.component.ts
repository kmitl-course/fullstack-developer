import { Component, OnInit } from '@angular/core';
import {Hero} from "../interface/hero";
import {HeroService} from "../service/hero.service";
import {HeroSearch} from "../interface/hero-search";
import {Skill} from "../interface/skill";
import {SkillService} from "../service/skill.service";

@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.css']
})
export class HeroListComponent implements OnInit {

  heroSearchFrom:HeroSearch ={
    heroName:'',
    skillId:''
  };
  skills : Skill[] = [];
  heroes : Hero[] = [];

  constructor(private heroService:HeroService,
              private skillService:SkillService) { }

  ngOnInit(): void {
    // this.heroService.getHeroes().subscribe(heroes => {
    //   this.heroes = heroes.data;
    // });
    this.skillService.getSkills().subscribe(response=>{
      this.skills = response.data;
    });
  }

  onSubmit() {
    this.heroService.getHeroes(this.heroSearchFrom).subscribe(heroes => {
      this.heroes = heroes.data;
    });
  }
}
