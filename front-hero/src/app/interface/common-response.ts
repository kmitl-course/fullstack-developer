export interface CommonResponse {
  statusCode: string,
  message: string,
  data: any
}
