export interface Skill {
  skillId: string;
  name?: string;
  description?: string;
}
