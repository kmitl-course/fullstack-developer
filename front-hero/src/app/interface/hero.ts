export interface Hero {
  heroId: string;
  name: string;
  description?: string;
  skillId?:string;
  birthday?:string;
  skillName?:string;
}
