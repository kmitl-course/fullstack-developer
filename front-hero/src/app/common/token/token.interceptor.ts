import { Injectable } from "@angular/core";
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest
} from "@angular/common/http";
import { Observable } from "rxjs";
import {finalize, tap} from "rxjs/operators";
import {AuthService} from "../auth/auth.service";


@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private authService:AuthService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let token = this.authService.getToken();
    if(token === null){
      token = '';
    }
    const authReq = req.clone({
      headers: new HttpHeaders({
        'Token':  token,
      })
    });
    return next.handle(authReq).pipe(tap(event=>{

    },error=>{
      if (error instanceof HttpErrorResponse) {
        if(error.status === 401){
          this.authService.logout();
        }
      }
    }));
  }


}
